# Setup #

An unofficial API for accessing stories on FanFiction.net. Just drop "story.php" on a php server and it's ready to go. No dependencies whatsoever!

### Features ###

* Easily get the title, summary, length, chapters, author id/name, and text of a story on fanfiction.net
* Download the entire work, or just download a single chapter

### API Demo ###
* http://kevinzhang.info/labs/ffexpt.php
* http://kevinzhang.info/labs/api_demo.php?story_id=8177168
* http://kevinzhang.info/labs/story.php?story_id=8177168&chapter=1
* Please don't overload my personal site! Set up your own server! (000webhost, etc.)
![FF.net API Demo.png](https://bitbucket.org/repo/dAqo5x/images/725089152-FF.net%20API%20Demo.png)

# API Documentation #

### story.php ###

* ../story.php?story_id=!!!!!!&chapter=!!!!!!
* story_id: fanfiction.net/s/[!!!story_id!!!]/
* chapter: optional. if not set, all chapters are returned