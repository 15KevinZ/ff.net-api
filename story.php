<?php
	if (isset($_GET['story_id'])) {
		$story_id = $_GET['story_id'];
		$story_html = file_get_contents("https://www.fanfiction.net/s/$story_id");

		preg_match("/<b class='xcontrast_txt'>(.*?)<\/b>/", $story_html, $results);
		if(!isset($results[1]))
			die('error: could_not_get_story_title');
		$story_title = $results[1];

		preg_match("/<div style='margin-top:2px' class='xcontrast_txt'>(.*?)<\/div>/", $story_html, $results);
		if(!isset($results[1]))
			die('error: could_not_get_story_summary');
		$story_summary = $results[1];

		preg_match("/Words: (.*?) - /", $story_html, $results);
		if(!isset($results[1]))
			die('error: could_not_get_story_words');
		$story_words = $results[1];

		preg_match("/Chapters: (.*?) - /", $story_html, $results);
		if(!isset($results[1]))
			die('error: could_not_get_story_chapters');
		$story_chapters = (int)$results[1];

		preg_match("/href='\/u\/(.*?)\//", $story_html, $results);
		if(!isset($results[1]))
			die('error: could_not_get_author_id');
		$author_id = $results[1];
		
		preg_match("/href='\/u\/(.*?)\/(.*?)'/", $story_html, $results);
		if(!isset($results[2]))
			die('error: could_not_get_author_name');
		$author_name = $results[2];

	}
	else
		die('error: could_not_get_story_id');

	$json_output = '';
	$json_output .= '{';
		$json_output .= '"story_id": "' . $story_id . '", ';
		$json_output .= '"story_title": "' . $story_title . '", ';
		$json_output .= '"story_summary": "' . $story_summary . '", ';

		$json_output .= '"story_words": "' . $story_words . '", ';
		$json_output .= '"story_chapters": "' . $story_chapters . '", ';

		$json_output .= '"author_id": "' . $author_id . '", ';
		$json_output .= '"author_name": "' . $author_name . '", ';

		$json_output .= '"chapters": {';
		
		for ($chapter = 1; $chapter <= $story_chapters; $chapter++) {
			if (isset($_GET['chapter'])) {
				$chapter = (int)$_GET['chapter'];
				$story_chapters = (int)$_GET['chapter'];
				if ($chapter == 0)
					break;
			}
			
			$chapter_html = file_get_contents("https://www.fanfiction.net/s/$story_id/$chapter");

			preg_match("/selected>(\d+). (.*?)</", $chapter_html, $results);
			if(!isset($results[2]))
				die('error: could_not_get_chapter_'.$chapter.'_title');
			$chapter_title = $chapter . '. ' . $results[2];

			preg_match("/storytext'>(.*?)<\/div>/s", $chapter_html, $results);
			if(!isset($results[1]))
				die('error: could_not_get_chapter_'.$chapter.'_text');
			$chapter_text = $results[1];
			$chapter_text = str_replace(PHP_EOL, ' ', $chapter_text);
			$chapter_text = str_replace('><', '>\n<', $chapter_text);
			$chapter_text = strip_tags($chapter_text);
			$chapter_text = str_replace('\n', '<br/><br/>', $chapter_text);
			$chapter_text = preg_replace('/<br\/>\s*<br\/>\s*<br\/>/', '<br/><br/>', $chapter_text);
			$chapter_text = preg_replace('/<br\/>\s*<br\/>\s*<br\/>/', '<br/><br/>', $chapter_text);
			$chapter_text = preg_replace('/<br\/>\s*<br\/>\s*<br\/>/', '<br/><br/>', $chapter_text);
			$chapter_text = preg_replace('/<br\/>\s*<br\/>\s*<br\/>/', '<br/><br/>', $chapter_text);
			$chapter_text = preg_replace('/<br\/>\s*<br\/>\s*<br\/>/', '<br/><br/>', $chapter_text);
			$chapter_text = trim($chapter_text, '<br/>');
			$chapter_text = trim($chapter_text);
			$chapter_text = htmlspecialchars($chapter_text, ENT_QUOTES);

			$json_output .= '"' . $chapter_title . '" : ' . '"' . $chapter_text . '", ';
		}
  		$json_output = rtrim($json_output, ', ');
  		$json_output .= '}';

	$json_output .= '}';

	print_r($json_output);
?>