<html>
<head>
	<title>FanFiction API Demo</title>
	<style>
		body {
			margin: auto;
			min-width: 360px;
			width: 95%;
			max-width: 940px;
		}
		
		#story-details {
			margin: 2px;
			padding: 2px;
			border-radius: 4px;
			background-color: #ddd;
			text-align: center;
		}
		
			#story-details * {
				padding: 0px;
				margin: 1px;
			}
		
		#chapter-picker {
			width: 22.5%;
			float: left;
			position: relative;
		}

			#chapter-list h2 {
				margin: 2px;
				padding: 2px;
			}
			
			#chapter-list .chapter-title {
				margin: 2px;
				padding: 2px;
				border-radius: 4px;
				background-color: #ddd;
				cursor: pointer;
				text-align: center;
			}
			
			#chapter-list .chapter-title:hover {
				background-color: #dde;
			}
			#chapter-list .chapter-title.selected {
				background-color: #eee;
			}
			
		#chapter-display {
			width: 72.5%;
			float: left;
				margin: 2px;
				padding: 2px;
				border-radius: 4px;
				background-color: #ddd;
			
			font-size: 1.0em;
			text-align: justify;
		}

			#chapter-display .chapter-text {
				display: none;
				padding: 2px;
				overflow-x: hidden;
			}
			#chapter-display .chapter-text.selected {
				display: block;
			}

		@media (max-width: 740px) {
			#chapter-picker {
				position: fixed;
				margin: 2px;
				padding: 2px;
				border-radius: 4px;
				min-width: 360px;
				width: 95%;
				max-width: 940px;
				background-color: white;
			}
			#chapter-picker:hover #chapter-list {
				display: block;
			}
			
			#chapter-list {
				display: none;
			}
			
			#chapter-display {
				width: 100%;
				padding-top: 4em;
			}
			
		}
			
	</style>
	<script src="jquery-1.11.1.min.js"></script>
	<script>
		var request;

		function decodeHTML(encoded) {
			return $("<div/>").html(encoded).text();
		}
		function showChapter(chapter) {
			$("html, body").animate({ scrollTop: 0 }, "fast");
			$('.chapter-title').removeClass('selected');
			$('.chapter-title[chapter=' + chapter + ']').addClass('selected');
			$('.chapter-text').removeClass('selected');
			$('.chapter-text[chapter=' + chapter + ']').addClass('selected');
		}
		function loadStory(story_id) {
			console.log('trying to get story_id: ' + story_id);
			if (typeof request != 'undefined')
				request.abort();
			request = $.ajax({
				url: "story.php",
				data: {
					story_id: story_id
				},
				success: function(data) {
					story = JSON.parse(data);
					console.log('loaded story: ' + story.story_title);
					$('#story-title').text(story.story_title);
					$('#author-name').text(story.author_name);
					
					var chapter = 1;
					$('#chapter-list').html('');
					$('#chapter-display').html('');
					for (var chapter_title in story.chapters) {
						$('#chapter-list').append('<div chapter="' + chapter + '" class="chapter-title">' + decodeHTML(chapter_title) + '</div>');
						$('#chapter-display').append('<div chapter="' + chapter + '" class="chapter-text">' + decodeHTML(story.chapters[chapter_title]) + '</div>');
						chapter++;
					}
					
					$('.chapter-title').click(function() {
						var chapter = $(this).attr('chapter');
						showChapter(chapter);
					});
					$('.chapter-title').first().click();
				}
			});
		}
		loadStory(<?php print $_GET['story_id']; ?>);

		$(document).ready(function() {
			$(document).ajaxStart(function() {
				$('#loading').show();
			});
			$(document).ajaxStop(function() {
				$('#loading').hide();
			});
		});
	</script>
</head>
<body>
	<div id="loading" style="position: fixed; z-index: 5; background-color: #dde; padding: 24px 12px; border-radius: 4px; top: 40%; left: 45%;"><h1>loading fanfic...</h1></div>
	<div id="chapter-picker">
		<div id="story-details">
			<h3 id="story-title"></h3>
			<h4>by <span id="author-name"></span></h4>
		</div>
		<div id="chapter-list">
		</div>
		<!-- <div id="chapter-jump" style="background-color: #ddd;">
			<input type="button" value="<" style="float: left;" onclick="showChapter(parseInt($('.chapter-title.selected').attr('chapter')) - 1);"></input>
			<input type="button" value=">" style="float: right;" onclick="showChapter(parseInt($('.chapter-title.selected').attr('chapter')) + 1);"></input>
			<br style="clear: both;"/>
		</div>-->
	</div>
	<div id="chapter-display">

	</div>
</body>
</html>