<html>
<head>
	<title>FanFiction.Net Exporter</title>
	<script src="jquery-1.11.1.min.js"></script>
	<script>
		function decodeHTML(encoded) {
			return $("<div/>").html(encoded).text();
		}
		function selectText(containerid) {
        	if (document.selection) {
           	var range = document.body.createTextRange();
            range.moveToElementText(document.getElementById(containerid));
            range.select();
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(document.getElementById(containerid));
            window.getSelection().addRange(range);
        }
    	}
		$(document).ready(function() {
			$('#loading').hide();
			$(document).ajaxStart(function() {
				$('#loading').show();
			});
			$(document).ajaxStop(function() {
				$('#loading').hide();
			});
			$('#submit').click(function() {
				$('#output').val('');
				$.ajax({
					url: "story.php",
					data: {
						story_id: $('#input').val()
					},
					success: function(data) {
						story = JSON.parse(data);
						console.log('loaded story: ' + story.story_title);
						
						var chapter = 1;
						for (var chapter_title in story.chapters) {
							$('#output').append(decodeHTML(story.chapters[chapter_title]));
							chapter++;
						}
						selectText('output');
					}
				});
			});
		});
	</script>
</head>
<body>
	<p>
		<input id="input" type="text" placeholder="story id..."></input>
		<input id="submit" type="submit" value="export"></input>
		<span id="loading">loading...</span>
	</p>
	<div id="output" style="width: 100%; height: 95%;"></div>
</body>
</html>